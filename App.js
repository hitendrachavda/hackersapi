/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, SafeAreaView} from "react-native"
import { createStackNavigator } from '@react-navigation/stack';
import { StoriesContainer } from './src/containers/StoriesContainer';
import { WebUrlContainer } from './src/containers/WebUrlContainer';

import { NavigationContainer } from '@react-navigation/native';

import 'react-native-gesture-handler';

const Stack = createStackNavigator();

const App = () => {
  return (
    <SafeAreaView style={styles.sectionContainer}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Stories">
        <Stack.Screen name="Stories" component={StoriesContainer} options=
        {{headerShown: false}} 
        />
        <Stack.Screen name="WebUrl" component={WebUrlContainer} />
      </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    backgroundColor :'white', 
    flex:1
  }

});

export default App;
