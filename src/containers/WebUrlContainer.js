import React from 'react';
import { WebView } from 'react-native-webview';
import { useRoute } from '@react-navigation/native';

export const WebUrlContainer = () => {
    const route = useRoute();
    console.log("url is ",route.params)
    return (
        <WebView source={{ uri: route.params.url }} />
    );
  };
  