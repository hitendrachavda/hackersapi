import React, { useEffect, useState } from 'react';
import { getStoryIds } from '../services/HackerApi';
import { Story } from '../components/Story';
import {Text,View, FlatList,StyleSheet} from "react-native"

export const StoriesContainer = ({navigation}) => {
  const [storyIds, setStoryIds] = useState([]);

  useEffect(() => {
    getStoryIds().then(data => {
      setStoryIds(data)
    });
  }, []);

  const loadMoreDate = () =>{
    setStoryIds([...storyIds, storyIds]);
  }

  const renderItem = ({item})=>(
      <Story key={item} storyId={item} navigation={navigation}/>
  );

  return (
    <View>
        <View style={styles.headerTitleStyle}>
        <Text style={styles.titleStyle}>Hacker News Stories</Text>
        </View>
        <FlatList
          data={storyIds}
          renderItem={renderItem}
          keyExtractor={(item) => item}
          onEndReached={loadMoreDate}
          onEndReachedThreshold={0.5}
          />
    </View>
  );
};

const styles = StyleSheet.create({
  headerTitleStyle : {
    backgroundColor : "#ff6600"
  },
  titleStyle : {
    marginVertical:10,
    fontSize : 20,
    fontWeight : '500',
    paddingLeft:10
  }
});