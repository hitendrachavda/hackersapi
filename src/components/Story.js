import React, { useState, useEffect, memo } from 'react';
import { Text , View, StyleSheet, TouchableOpacity } from "react-native"
import { getStory } from '../services/HackerApi';
import { mapTime } from '../mappers/mapTime';

export const Story = memo(function Story({ storyId , navigation }) {
  const [story, setStory] = useState({});

  useEffect(() => {
    getStory(storyId).then(data => data && data.url && setStory(data));
  }, []);

  return story && story.url ? (
      <TouchableOpacity style={styles.container} onPress={()=>{
        console.log("story.url",story.url)
        navigation.navigate('WebUrl',{ url: story.url })
      }}>
        <View style={styles.datacontainerstyle}>
        <View >
          <Text style={{fontSize : 18}}>{story.title}</Text>
        </View>
        <View style={styles.moreInfoStyle}>
          <Text style={{color:'gray'}}>{"By:"+ story.by }</Text>
          <Text style={{color:'gray'}}>{"Posted:" + mapTime(story.time) }</Text>
        </View>
        </View>
        <View style={{height:0.3,backgroundColor:"black",width:'100%'}}></View>
    </TouchableOpacity>
  ) : null;
});

const styles = StyleSheet.create({
  container : {
    backgroundColor:"#f6f6ef"
  },
  datacontainerstyle : {
    marginHorizontal:10,
    paddingVertical : 5
  },
  moreInfoStyle : {
    flexDirection:'row',
    justifyContent:'space-between'
  }
});